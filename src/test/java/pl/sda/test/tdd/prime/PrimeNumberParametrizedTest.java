package pl.sda.test.tdd.prime;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.prime.PrimeNumber;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)

public class PrimeNumberParametrizedTest {
    private int number;
    private boolean isPrimeNumber;

    public PrimeNumberParametrizedTest(int number, boolean isPrimeNumber) {
        this.number = number;
        this.isPrimeNumber = isPrimeNumber;
    }
    @Test
    public void isPrimeNumber(){
        assertEquals(isPrimeNumber, PrimeNumber.isPrimeNumber(  number));
    }

    @Parameterized.Parameters(name = "{index} should be {0} be a Prime Number {1}")
    public static Collection<Object[]> dataProvider(){
        return Arrays.asList(new Object[][] {
                {1,false},
                {0,false},
                {4,false},
                {3,true},
                {9,false},
                {2,true} });
    }
}