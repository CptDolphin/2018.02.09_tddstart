package pl.sda.test.tdd.prime;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.prime.PrimeNumberv2;

import java.util.Arrays;
import java.util.Collection;

@RunWith(Parameterized.class)
public class PrimeNumberParametrizedTestv2 {
    private int number;
    private boolean isItAPrimeNumber;

    public PrimeNumberParametrizedTestv2(int number, boolean isItAPrimeNumber) {
        this.number = number;
        this.isItAPrimeNumber = isItAPrimeNumber;
    }

//    @Test
//    public void areThoseNumbersPrime() {
//        Assert.assertArrayEquals(new boolean[]{isItAPrimeNumber}, PrimeNumberv2.isPrimeNumber(number));
//    }

    @Parameterized.Parameters(name = "index: [{index}]; is number {1} a Prime number? Answer: [0}")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {1, false},
                {2, true},
                {3, true},
                {4, false},
                {5, true},
                {6, false},
                {9, false},
        });
    }
}
