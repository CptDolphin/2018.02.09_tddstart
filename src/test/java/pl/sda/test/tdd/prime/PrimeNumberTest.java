package pl.sda.test.tdd.prime;

import org.junit.Test;
import pl.sda.tdd.prime.PrimeNumber;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PrimeNumberTest {
    @Test
    public void threeShouldBePrimeNumber() {
        assertTrue(PrimeNumber.isPrimeNumber(3));
    }

    @Test
    public void zeroShouldNotBePrime() {
        assertFalse(PrimeNumber.isPrimeNumber(0));
    }

    @Test
    public void minusTwoShouldNotBePrime() {
        assertFalse(PrimeNumber.isPrimeNumber(-2));
    }

    @Test
    public void fourShouldNotBePrimeNumber() {
        assertFalse(PrimeNumber.isPrimeNumber(4));
    }

    @Test
    public void ninjeShouldNotBePrimeNumber(){
        assertFalse(PrimeNumber.isPrimeNumber(9));
    }
}
