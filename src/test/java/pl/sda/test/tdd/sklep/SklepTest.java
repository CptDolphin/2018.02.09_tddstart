package pl.sda.test.tdd.sklep;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.Sklep.Klient;
import pl.sda.tdd.Sklep.Sklep;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class SklepTest {

    @Parameterized.Parameter(value = 0)
    public int a;
    @Parameterized.Parameter(value = 1)
    public int b;
    @Parameterized.Parameter(value = 2)
    public int c;


    @Test
    public void czyZaPiecdziesiatZlotyDostanieJedenKupon() {
        assertEquals(1, Sklep.zaIleKupuje(new Klient(50, "Tomek")));
    }

    @Test
    public void czyZaOsiemdziesiatZlotyDostanieJedenKupon() {
        assertEquals(2, Sklep.zaIleKupuje(new Klient(80, "Robert")));
    }

    @Test
    public void czyZaStoTrzydziesciZlotyDostanieJedenKupon() {
        assertEquals(3, Sklep.zaIleKupuje(new Klient(130, "Czesio")));
    }


}
