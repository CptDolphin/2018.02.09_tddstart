package pl.sda.test.tdd.delta;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.delta.QuadraticEquation;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class QuadraticEquationParamTest {

    @Parameterized.Parameter(value = 0)
    public int a;

    @Parameterized.Parameter(value = 1)
    public int b;

    @Parameterized.Parameter(value = 2)
    public int c;

    @Parameterized.Parameter(value = 3)
    public int[] expected;

    @Parameterized.Parameters(name = "[{index}] should {0}x^2 + {1}x + {2} return = {3}")
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {1, 6, 9, new int[]{-3}},
                {-1, 3, 4, new int[]{4,-1}},
                {1, 2, 5, new int[]{}},
                {1, 2, -3, new int[]{-3,1}}});
    }

    @Test
    public void quadraticEquationTest() {
        assertArrayEquals(expected, QuadraticEquation.calculate(a, b, c));
    }
}