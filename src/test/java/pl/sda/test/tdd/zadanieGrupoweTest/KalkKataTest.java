package pl.sda.test.tdd.zadanieGrupoweTest;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.sda.tdd.zadanieGrupowe.KalkKata;

import static org.junit.Assert.assertEquals;

public class KalkKataTest {

    @Ignore
    @Test
    public void dlaPustegoStringZwracaZero() {
        assertEquals(0, KalkKata.kalkAdd(""));
    }

    @Ignore
    @Test(expected = IllegalArgumentException.class)
    public void wiecejNizDwieLiczbyZwracajaWyjatek() {
        KalkKata.kalkAdd("1,2,3");
    }

    @Test
    public void metodaZwracaSumewprowadzonychLiczb() {
        assertEquals(10, KalkKata.kalkAdd("3,6,1"));//1,2\n5 ->8
    }

    @Test(expected = IllegalArgumentException.class)
    public void dlaZeroZwracaWyjatek() {
        KalkKata.kalkAdd("");
    }

    @Test
    public void znakiNowejLinii(){
        assertEquals(8,KalkKata.kalkAdd("1,3\n4"));
    }
}
