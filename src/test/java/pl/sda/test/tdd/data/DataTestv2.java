package pl.sda.test.tdd.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.sda.tdd.data.ObliczWiekv2;

public class DataTestv2 {
    @Rule
    public ExpectedException exception = ExpectedException.none();

    //nieprawidlowy format wybranej daty
    @Test
    public void nieprawidlowyFormatDatyZwracaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Wybrana Data");
        ObliczWiekv2.obliczWiek("2000-05-04", "201k5-05-03");
    }

    //nieprawidolowy format urodzin
    @Test
    public void nieprawidlowyFormatUrodzin() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Data Urodzenia");
        ObliczWiekv2.obliczWiek("200j-05-045", "2015-05-03");
    }


    //data urodzenia mniejsza od daty wybranej
    @Test
    public void dataUrodzeniaMniejszaOdDatyWybranej() {

    }
//
//    //data urodzenia wieksza od wybranje daty nie daje wyjatku
//    @Test

    //osoba urodzona 5 lat pozniej ma 5 lat
    @Test
    public void osobaUrodza5LatPozniejMa5Lat(){
        ObliczWiekv2.obliczWiek("2000-05-04", "2015-05-03");
    }
}
