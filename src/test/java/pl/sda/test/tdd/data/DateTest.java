package pl.sda.test.tdd.data;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pl.sda.tdd.data.ObliczWiek;

import java.util.function.ObjLongConsumer;

import static org.junit.Assert.assertEquals;

public class DateTest {

    @Rule
    public ExpectedException exception = ExpectedException.none();

    @Test
    public void nieprawidolyFormatDatyUrodzaneiZwracaWyjatek() {
        exception.expect((IllegalArgumentException.class));
        exception.expectMessage("Data Urodzenia");
        ObliczWiek.obliczWiek("2015j-05-01", "2010-06-02");
    }

    @Test
    public void nieprawidlowyFormatWybranejDatyZwracaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Wybrana Data");
        ObliczWiek.obliczWiek("2000-05-01", "30-12-2000");
    }

    @Test
    public void dataUrodzeniaMniejszaOdWybranejDatyZwracaWyjatek() {
        exception.expect(IllegalArgumentException.class);
        exception.expectMessage("Data Urodzenia musi byc mniejsza lub rowna wybraje dacie");
        ObliczWiek.obliczWiek("2000-05-01", "1999-01-01");
    }

    @Test
    public void dataUrodzeniaWiekszaOdWybranejDatyNiZwracaWyjatku() {
        ObliczWiek.obliczWiek("2010-01-01", "2018-01-01");
    }

    @Test
    public void osobaUrodzona5LatPozniejMa5lat() {
        assertEquals(5, ObliczWiek.obliczWiek("2000-01-01", "2005-02-01"));
    }

    @Test
    public void dzienPrzedUrodzinamiOsobaMaMniejLatNizJejRocznik(){
        assertEquals(9,ObliczWiek.obliczWiek("2000-05-10","2010-05-09"));
    }
}