package pl.sda.tdd.Sklep;

public class Klient {
    private String imie;
    private int zaIleKupuje;
    private int kupon;

    public Klient(int zaIleKupuje, String imie) {
        this.zaIleKupuje = zaIleKupuje;
        this.imie = imie;
        this.kupon = 0;
    }

    public int getKupon() {
        return kupon;
    }

    public void setKupon(int kupon) {
        this.kupon = kupon;
    }

    public String getImie() {
        return imie;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public int getZaIleKupuje() {
        return zaIleKupuje;
    }

    public void setZaIleKupuje(int zaIleKupuje) {
        this.zaIleKupuje = zaIleKupuje;
    }
}
