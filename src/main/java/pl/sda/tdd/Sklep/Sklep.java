package pl.sda.tdd.Sklep;

public class Sklep {
    private Klient klient;

    public Sklep(Klient klient) {
        this.klient = klient;
    }

    public Sklep() {
    }

    public Klient getKlient() {
        return klient;
    }

    public void setKlient(Klient klient) {
        this.klient = klient;
    }

    public static int zaIleKupuje(Klient klient) {
        int ileKlientPlaci = klient.getZaIleKupuje();
        if (ileKlientPlaci < 40) return 0;
        else if (ileKlientPlaci >= 40 && ileKlientPlaci < 80) {
            klient.setKupon(1);
            System.out.println("Klient: " + klient.getImie() + " dostaje " + klient.getKupon() + " kupon na darmowe wino");
            return klient.getKupon();
        } else if (ileKlientPlaci >= 80 && ileKlientPlaci < 120) {
            klient.setKupon(2);
            System.out.println("Klient: " + klient.getImie() + " dostaje " + klient.getKupon() + " kupon na darmowe wino");
            return klient.getKupon();
        } else {
            klient.setKupon(3);
            System.out.println("Klient: " + klient.getImie() + " dostaje " + klient.getKupon() + " kupon na darmowe wino");
            return klient.getKupon();
        }
    }
}
