package pl.sda.tdd.Sklep;

public class Main {
    public static void main(String[] args) {
        Klient klient1 = new Klient(50, "tomek");
        Klient klient2 = new Klient(100,"robert");
        Klient klient3 = new Klient(130, "czesio");

        Sklep sklep = new Sklep();

        System.out.println(sklep.zaIleKupuje(klient1));
        System.out.println(sklep.zaIleKupuje(klient2));
        System.out.println(sklep.zaIleKupuje(klient3));
    }
}
