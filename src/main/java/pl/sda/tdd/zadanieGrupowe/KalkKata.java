package pl.sda.tdd.zadanieGrupowe;

import java.util.Arrays;

public class KalkKata {

    /**
     * kalkulator zwraca sume wprowadzonch liczb
     *
     * @param liczby - liczby oddzielone przecinkiem (warunek minimalna ilosc to 1 )
     * @return
     */
    public static int kalkAdd(String liczby) {
        if (liczby.equals("")) {
            throw new IllegalArgumentException();
        }
//        String nowySeparator;
//        if(liczby.substring(0,2).equals("//")){
//            //           //;\n5;7;8;4;3;6;8
//            nowySeparator=liczby.substring(2,3);
//        }
        int[] listaIntow = Arrays.stream(liczby.split("\\D")).mapToInt(Integer::parseInt).toArray();
        int wynik = 0;
        if (listaIntow.length == 1) return listaIntow[0];
        for (int i = 0; i < listaIntow.length; i++) {
            wynik += listaIntow[i];
        }
        return wynik;
    }
}