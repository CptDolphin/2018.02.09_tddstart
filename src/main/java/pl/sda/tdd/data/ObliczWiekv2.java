package pl.sda.tdd.data;

public class ObliczWiekv2 {

    public static final String FORMAT_DATY = "\\d{4}-\\d{2}-\\d{2}";

    public static int obliczWiek(String dataUrodzenia, String wybranaData) {
        sprawdzFormatDaty(dataUrodzenia, wybranaData);
        int wiek = obliczCzas(dataUrodzenia, wybranaData);
        if (wiek < 0) {
            throw new IllegalArgumentException("Urodziny musza byc przed data koncowa");
        } else return wiek;
    }

    private static int obliczCzas(String dataUrodzenia, String wybranaData) {
        int urodzenie = Integer.parseInt(dataUrodzenia.substring(0,4));
        int dataKoncowa = Integer.parseInt(wybranaData.substring(0,4));
        return dataKoncowa - urodzenie;
    }

    private static void sprawdzFormatDaty(String dataUrodzenia, String wybranaData) {
        if (!dataUrodzenia.equals(FORMAT_DATY)) {
            throw new IllegalArgumentException("Data Urodzenia");
        }
        else if (!wybranaData.equals(FORMAT_DATY)) {
            throw new IllegalArgumentException("Wybrana Data");
        }
    }
}
