package pl.sda.tdd.data;

public class ObliczWiek {

    public static final String FORMAT_DATY = "\\d{4}-\\d{2}-\\d{2}";

    /**
     * Oblicza wiek osoby w danym dniu
     *
     * @param dataUrodzenia - data w formacie YYYY-MM-DD
     * @param wybranaData   - data w formacie YYYY-MM-DD
     *                      wymagane: dataUrodzenia < wybranaData
     * @return
     */
    public static int obliczWiek(String dataUrodzenia, String wybranaData) {
        sprawdzFormatDat(dataUrodzenia, wybranaData);
        int wiek = zwrocWiek(dataUrodzenia, wybranaData);
        sprawdzPoprawnoscWieku(wiek);
        return wiek;

    }
    private static void sprawdzPoprawnoscWieku(int wiek){
        if (wiek < 0) {
            throw new IllegalArgumentException("Data Urodzenia musi byc mniejsza lub rowna wybraje dacie");
        }
    }
    private static void sprawdzFormatDat(String dataUrodzenia, String wybranaData) {
        if (!dataUrodzenia.matches(FORMAT_DATY)) {
            throw new IllegalArgumentException("Data Urodzenia");
        }
        if (!wybranaData.matches(FORMAT_DATY)) {
            throw new IllegalArgumentException("Wybrana Data");
        }
    }

    private static int zwrocWiek(String dataUrodzenia, String wybranaData) {
        int wiek = 0;

        int rokUrodzenia = zwrocWartoscDaty(dataUrodzenia, TYP_DATY.ROK);
        int wybranyRok = zwrocWartoscDaty(wybranaData, TYP_DATY.ROK);

        int miesiacUrodzenia = zwrocWartoscDaty(dataUrodzenia, TYP_DATY.MIESIAC);
        int wybranyMiesiac = zwrocWartoscDaty(wybranaData, TYP_DATY.MIESIAC);


        if (wybranyMiesiac < miesiacUrodzenia) {
            wiek--;
        } else if (wybranyMiesiac == miesiacUrodzenia) {

            int dzienUrodzenia = zwrocWartoscDaty(dataUrodzenia, TYP_DATY.DZIEN);
            int dzienWybrany = zwrocWartoscDaty(wybranaData, TYP_DATY.DZIEN);
            if (dzienWybrany < dzienUrodzenia) {
                wiek--;
            }
        }
        wiek += wybranyRok - rokUrodzenia;
        return wiek;
    }

    private static int zwrocWartoscDaty(String data, TYP_DATY typ_daty) {
        return Integer.parseInt(data.substring(typ_daty.pobierzPierwszyIndex(), typ_daty.pobierzDrugiIndex()));
    }
}

enum TYP_DATY {
    ROK(0, 4),
    MIESIAC(5, 7),
    DZIEN(8, 10);

    private final int pierwszyIndex;
    private final int drugiIndex;

    TYP_DATY(int pierwszyIndex, int drugiIndex) {
        this.pierwszyIndex = pierwszyIndex;
        this.drugiIndex = drugiIndex;
    }

    public int pobierzPierwszyIndex() {
        return pierwszyIndex;
    }

    public int pobierzDrugiIndex() {
        return drugiIndex;
    }
}

//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//        LocalDate data1 = LocalDate.parse(LocalDate.parse(dataUrodzenia).format(formatter));
//        LocalDate data2 = LocalDate.parse(LocalDate.parse(wybranaData).format(formatter));
//        return Period.between(data1,data2).getYears();
