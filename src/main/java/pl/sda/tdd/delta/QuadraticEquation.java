package pl.sda.tdd.delta;

public class QuadraticEquation {
    public static int[] calculate(int a, int b, int c) {
        int[] tab = new int[2];
        int delta = (int) (Math.pow(b,2) - 4*a*c);
        if(delta<0){
            return new int[0];
        }else if(delta==0){
            return new int[]{(-b/2*a)};
        }else if(delta>0){
            tab[0] = (int) (((-b)-(Math.sqrt(delta)))/2*a);
            tab[1] = (int) (((-b)+(Math.sqrt(delta)))/2*a);
        }
        return tab;
    }

    public static void main(String[] args) {
        //testy czy dziala
        System.out.println(calculate(1,6,9));
        System.out.println(calculate(-1,3,4));
        System.out.println(calculate(1,2,5));
        System.out.println(calculate(1,2,-3));
    }
}