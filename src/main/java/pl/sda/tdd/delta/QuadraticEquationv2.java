package pl.sda.tdd.delta;

public class QuadraticEquationv2 {
    public static int[] testowanieDelty(int a, int b, int c) {
        int delta = obliczanieDelty(a, b, c);
        if (delta < 0) return new int[0];
        else {
            int x1 = (int) ((-b - Math.sqrt(delta)) / (2 * a));
            int x2 = (int) ((-b + Math.sqrt(delta)) / (2 * a));
            if (x1 == x2) return new int[]{-b / 2 * a};
            else return new int[]{x1, x2};
        }
    }

    private static int obliczanieDelty(int a, int b, int c) {
        return b * b - 4 * a * c;
    }
}
